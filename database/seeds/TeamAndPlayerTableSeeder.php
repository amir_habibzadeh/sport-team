<?php

use Illuminate\Database\Seeder;

class TeamAndPlayerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Team::class, 4)
            ->create()
            ->each(function (\App\Team $team) {
                factory(App\Player::class, 11)->create([
                   'team_id' => $team->id
                ]);
            });

        $this->command->info('4 team with 11 player for each of them created.');
    }
}
