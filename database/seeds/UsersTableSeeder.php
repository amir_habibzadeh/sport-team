<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            factory(App\User::class)->create();
        } catch (\Illuminate\Database\QueryException $exception) {
            $this->command->error('Users Table Seeded Before!');
        }
        $this->command->warn('Username: user@user.com');
        $this->command->warn('Password: secret');
    }
}
