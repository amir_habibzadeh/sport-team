<?php

namespace App\Http\Controllers;

use App\Http\Resources\PlayerCollection;
use App\Player;
use App\Team;
use Illuminate\Http\Request;

class TeamPlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($teamId)
    {
        return PlayerCollection::collection(Player::where('team_id', $teamId)->orderByDesc('id')->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return PlayerCollection|\Illuminate\Http\JsonResponse
     */
    public function store($teamId, Request $request)
    {
        $validator = \Validator::make($data = $request->only([
            'first_name',
            'last_name'
        ]), [
            'first_name' => 'required|min:3|max:255',
            'last_name' => 'required|min:3|max:255'
        ]);

        if ($validator->fails()) {
           return response()->json([
               [
                   'message' => 'validation failed',
                   'errors' => $validator->errors()
               ]
           ], 422);

        }
        $team = Team::findOrFail($teamId);

        return new PlayerCollection($team->players()->create($data));
    }

    /**
     * Display the specified resource.
     *
     * @param $teamId
     * @param  int $id
     * @return PlayerCollection
     */
    public function show($teamId, $id)
    {
        $team = Team::findOrFail($teamId);

        return new PlayerCollection($team->players()->findOrFail($id));
    }


    /**
     * Update the specified resource in storage.
     * @param $teamId
     * @param $id
     * @param Request $request
     * @return PlayerCollection|\Illuminate\Http\JsonResponse
     */
    public function update($teamId, $id, Request $request)
    {
        $validator = \Validator::make($data = $request->only([
            'first_name',
            'last_name'
        ]), [
            'first_name' => 'required|min:3|max:255',
            'last_name' => 'required|min:3|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json([
                [
                    'message' => 'validation failed',
                    'errors' => $validator->errors()
                ]
            ], 422);

        }

        $team = Team::findOrFail($teamId);
        $player = $team->players()->findOrFail($id);
        $player->first_name = $data['first_name'];
        $player->last_name = $data['last_name'];
        $player->save();

        return new PlayerCollection($player);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $teamId
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($teamId, $id)
    {
        $team = Team::findOrFail($teamId);
        $player = $team->players()->findOrFail($id);
        $player->delete();

        return response(null, 204);
    }
}
