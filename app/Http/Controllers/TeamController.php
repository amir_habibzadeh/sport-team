<?php

namespace App\Http\Controllers;

use App\Http\Resources\TeamCollection;
use App\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return TeamCollection::collection(Team::orderByDesc('id')->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return TeamCollection|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($data = $request->only([
            'name'
        ]), [
            'name' => 'required|min:3|max:255',
        ]);

        if ($validator->fails()) {
           return response()->json([
               [
                   'message' => 'validation failed',
                   'errors' => $validator->errors()
               ]
           ], 422);

        }

        return new TeamCollection(Team::create($data));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return TeamCollection
     */
    public function show($id)
    {
        return new TeamCollection(Team::findOrFail($id));
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $id
     * @return TeamCollection|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($data = $request->only([
            'name'
        ]), [
            'name' => 'required|min:3|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                [
                    'message' => 'validation failed',
                    'errors' => $validator->errors()
                ]
            ], 422);

        }

        $team = Team::findOrFail($id);
        $team->name = $data['name'];
        $team->save();

        return new TeamCollection($team);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = Team::findOrFail($id);
        $team->delete();

        return response(null, 204);
    }
}
