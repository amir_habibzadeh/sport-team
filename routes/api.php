<?php
Route::middleware(['auth:api'])->group(function () {
    Route::resource('teams', 'TeamController');
    Route::resource('teams.players', 'TeamPlayerController');
});


